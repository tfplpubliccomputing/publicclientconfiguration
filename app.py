import os
import re
import subprocess

from flask import Flask, render_template, request, redirect

app = Flask(__name__)

_hostname = None
_session_server_ip = None
_prefixes = None
_express_prefix = None

_everyday = False
_everyday_hour = None
_everyday_minute = None
_everyday_ampm = None

_monday_hour = None
_monday_minute = None
_monday_ampm = None

_tuesday_hour = None
_tuesday_minute = None
_tuesday_ampm = None

_wednesday_hour = None
_wednesday_minute = None
_wednesday_ampm = None

_thursday_hour = None
_thursday_minute = None
_thursday_ampm = None

_friday_hour = None
_friday_minute = None
_friday_ampm = None

_saturday_hour = None
_saturday_minute = None
_saturday_ampm = None

_sunday_hour = None
_sunday_minute = None
_sunday_ampm = None

_update_day_of_week = None
_update_hour = None
_update_minute = None
_update_ampm = None


def write_settings(server_ip, prefixes, express_prefix):
    #  Remove existing settings.ini file.
    try:
        os.remove('/home/guest/.session_manager/settings.ini')
    except Exception as e:
        print(e)

    f = open('/home/guest/.session_manager/settings.ini', 'a')
    f.writelines(['[session]\n', 'SessionServer = %s\n' % server_ip, 'allowedPrefixes = %s\n' % prefixes, 'expressPrefix = %s\n' % express_prefix])
    f.close()


def write_shutdown_cron(cron_string):
    f = open('/etc/cron.d/shutdowns', 'a')
    f.write("%s\n" % cron_string)
    f.close()


def write_update_cron(cron_string):
    f = open('/etc/cron.d/updates', 'a')
    f.write("%s\n" % cron_string)
    f.close()


def remove_shutdown_cron_file():
    try:
        os.remove('/etc/cron.d/shutdowns')
    except Exception as e:
        print(e)


def remove_update_cron():
    try:
        os.remove('/etc/cron.d/updates')
    except Exception as e:
        print(e)


def convert_to_cron(hour, minute, ampm, command, day=None, everyday=None):
    if everyday is True:
        hour = int(hour)
        minute = int(minute)

        if hour > 12:
            return False

        if minute > 59:
            return False

        if ampm == 'pm':
            if hour < 12:
                hour = hour + 12

            if hour >= 24:
                hour = 12

        if ampm == 'am':
            if hour == 12:
                hour = 0

        return "#  %s\n%s %s * * * root %s \n\n" % ("Everyday", minute, hour, command)

    else:
        hour = int(hour)
        minute = int(minute)
        original_day = day

        if day not in ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']:
            return False

        if hour > 12:
            return False

        if minute > 59:
            return False

        if ampm == 'pm':
            if hour < 12:
                hour = hour + 12

            if hour >= 24:
                hour = 12

        if ampm == 'am':
            if hour == 12:
                hour = 0

        if day == 'sunday':
            day = "0"
        elif day == 'monday':
            day = "1"
        elif day == 'tuesday':
            day = "2"
        elif day == 'wednesday':
            day = "3"
        elif day == 'thursday':
            day = "4"
        elif day == 'friday':
            day = "5"
        elif day == 'saturday':
            day = "6"
        else:
            return False

        return "#  %s\n%s %s * * %s root %s \n\n" % (original_day.title(), minute, hour, day, command)


def set_hostname(hostname):
    hostname = re.sub('[^A-Za-z0-9- ]+', '', hostname.strip()).replace(" ", "-")
    subprocess.run(["hostnamectl", "set-hostname", "%s" % hostname])


@app.route('/')
def index():

    context = {}

    if _session_server_ip:
        context['session_server_ip'] = _session_server_ip

    if _prefixes:
        context['prefixes'] = _prefixes

    if _express_prefix:
        context['express_prefix'] = _express_prefix

    return render_template('index.html', context=context)


@app.route('/setup/settings', methods=['GET', 'POST'])
def settings():
    global _session_server_ip
    global _prefixes
    global _express_prefix
    global _hostname
    global _everyday

    if request.method == 'POST':
        hostname = request.form['hostname']
        server_ip = request.form['server_ip']
        prefixes = request.form['prefixes']
        express_prefix = request.form['express_prefix']

        _hostname = hostname
        _session_server_ip = server_ip
        _prefixes = prefixes
        _express_prefix = express_prefix

        # Set hostname
        try:
            set_hostname(hostname)
        except Exception as e:
            print(e)

        # Write settings.ini file
        try:
            write_settings(server_ip, prefixes, express_prefix)
        except Exception as e:
            print(e)

        if _everyday:
            return redirect('/setup/shutdown/everyday')
        else:
            return redirect('/setup/shutdown/custom')
    else:

        context = {}

        if _session_server_ip:
            context['session_server_ip'] = _session_server_ip

        if _prefixes:
            context['prefixes'] = _prefixes

        if _express_prefix:
            context['express_prefix'] = _express_prefix

        if _hostname:
            context['hostname'] = _hostname

        return render_template('index.html', context=context)


@app.route('/setup/shutdown/everyday', methods=['GET', 'POST'])
def cron_everyday():

    global _everyday_hour
    global _everyday_minute
    global _everyday_ampm
    global _everyday
    _everyday = True

    if request.method == 'POST':
        remove_shutdown_cron_file()

        everyday_hour = request.form['everyday_hour']
        _everyday_hour = everyday_hour

        everyday_minute = request.form['everyday_minute']
        _everyday_minute = everyday_minute

        everyday_ampm = request.form['everyday_ampm']
        _everyday_ampm = everyday_ampm

        cron_string = convert_to_cron(everyday_hour, everyday_minute, everyday_ampm, "init 0", everyday=True)

        try:
            write_shutdown_cron(cron_string)
        except Exception as e:
            print(e)

        return redirect('/setup/updates')

    else:
        context = {}

        if _everyday_hour:
            context['everyday_hour'] = _everyday_hour

        if _everyday_minute:
            context['everyday_minute'] = _everyday_minute

        if _everyday_ampm:
            context['everyday_ampm'] = _everyday_ampm

        return render_template('cron-everyday.html', context=context)


@app.route('/setup/shutdown/custom', methods=['GET', 'POST'])
def cron():

    global _everyday
    _everyday = False

    global _monday_hour
    global _monday_minute
    global _monday_ampm

    global _tuesday_hour
    global _tuesday_minute
    global _tuesday_ampm

    global _wednesday_hour
    global _wednesday_minute
    global _wednesday_ampm

    global _thursday_hour
    global _thursday_minute
    global _thursday_ampm

    global _friday_hour
    global _friday_minute
    global _friday_ampm

    global _saturday_hour
    global _saturday_minute
    global _saturday_ampm

    global _sunday_hour
    global _sunday_minute
    global _sunday_ampm

    if request.method == 'POST':

        remove_shutdown_cron_file()

        monday_hour = request.form['monday_hour']
        _monday_hour = request.form['monday_hour']

        monday_minute = request.form['monday_minute']
        _monday_minute = request.form['monday_minute']

        monday_ampm = request.form['monday_ampm']
        _monday_ampm = request.form['monday_ampm']

        cron_string = convert_to_cron(monday_hour, monday_minute, monday_ampm, "init 0", day='monday', everyday=False)

        try:
            write_shutdown_cron(cron_string)
        except Exception as e:
            print(e)

        tuesday_hour = request.form['tuesday_hour']
        _tuesday_hour = request.form['tuesday_hour']

        tuesday_minute = request.form['tuesday_minute']
        _tuesday_minute = request.form['tuesday_minute']

        tuesday_ampm = request.form['tuesday_ampm']
        _tuesday_ampm = request.form['tuesday_ampm']

        cron_string = convert_to_cron(tuesday_hour, tuesday_minute, tuesday_ampm, "init 0", day='tuesday', everyday=False)

        try:
            write_shutdown_cron(cron_string)
        except Exception as e:
            print(e)

        wednesday_hour = request.form['wednesday_hour']
        _wednesday_hour = request.form['wednesday_hour']

        wednesday_minute = request.form['wednesday_minute']
        _wednesday_minute = request.form['wednesday_minute']

        wednesday_ampm = request.form['wednesday_ampm']
        _wednesday_ampm = request.form['wednesday_ampm']

        cron_string = convert_to_cron(wednesday_hour, wednesday_minute, wednesday_ampm, "init 0", day='wednesday', everyday=False)

        try:
            write_shutdown_cron(cron_string)
        except Exception as e:
            print(e)

        thursday_hour = request.form['thursday_hour']
        _thursday_hour = request.form['thursday_hour']

        thursday_minute = request.form['thursday_minute']
        _thursday_minute = request.form['thursday_minute']

        thursday_ampm = request.form['thursday_ampm']
        _thursday_ampm = request.form['thursday_ampm']

        cron_string = convert_to_cron(thursday_hour, thursday_minute, thursday_ampm, "init 0", day='thursday', everyday=False)

        try:
            write_shutdown_cron(cron_string)
        except Exception as e:
            print(e)

        friday_hour = request.form['friday_hour']
        _friday_hour = request.form['friday_hour']

        friday_minute = request.form['friday_minute']
        _friday_minute = request.form['friday_minute']

        friday_ampm = request.form['friday_ampm']
        _friday_ampm = request.form['friday_ampm']

        cron_string = convert_to_cron(friday_hour, friday_minute, friday_ampm, "init 0", day='friday', everyday=False)

        try:
            write_shutdown_cron(cron_string)
        except Exception as e:
            print(e)

        saturday_hour = request.form['saturday_hour']
        _saturday_hour = request.form['saturday_hour']

        saturday_minute = request.form['saturday_minute']
        _saturday_minute = request.form['saturday_minute']

        saturday_ampm = request.form['saturday_ampm']
        _saturday_ampm = request.form['saturday_ampm']

        cron_string = convert_to_cron(saturday_hour, saturday_minute, saturday_ampm, "init 0", day='saturday', everyday=False)

        try:
            write_shutdown_cron(cron_string)
        except Exception as e:
            print(e)

        sunday_hour = request.form['sunday_hour']
        _sunday_hour = request.form['sunday_hour']

        sunday_minute = request.form['sunday_minute']
        _sunday_minute = request.form['sunday_minute']

        sunday_ampm = request.form['sunday_ampm']
        _sunday_ampm = request.form['sunday_ampm']

        cron_string = convert_to_cron(sunday_hour, sunday_minute, sunday_ampm, "init 0", day='sunday', everyday=False)

        try:
            write_shutdown_cron(cron_string)
        except Exception as e:
            print(e)

        return redirect('/setup/updates')

    else:

        context = {}

        if _monday_hour:
            context['monday_hour'] = _monday_hour

        if _monday_minute:
            context['monday_minute'] = _monday_minute

        if _monday_ampm:
            context['monday_ampm'] = _monday_ampm

        if _tuesday_hour:
            context['tuesday_hour'] = _tuesday_hour

        if _tuesday_minute:
            context['tuesday_minute'] = _tuesday_minute

        if _tuesday_ampm:
            context['tuesday_ampm'] = _tuesday_ampm

        if _wednesday_hour:
            context['wednesday_hour'] = _wednesday_hour

        if _wednesday_minute:
            context['wednesday_minute'] = _wednesday_minute

        if _wednesday_ampm:
            context['wednesday_ampm'] = _wednesday_ampm

        if _thursday_hour:
            context['thursday_hour'] = _thursday_hour

        if _thursday_minute:
            context['thursday_minute'] = _thursday_minute

        if _thursday_ampm:
            context['thursday_ampm'] = _thursday_ampm

        if _friday_hour:
            context['friday_hour'] = _friday_hour

        if _friday_minute:
            context['friday_minute'] = _friday_minute

        if _friday_ampm:
            context['friday_ampm'] = _friday_ampm

        if _saturday_hour:
            context['saturday_hour'] = _saturday_hour

        if _saturday_minute:
            context['saturday_minute'] = _saturday_minute

        if _saturday_ampm:
            context['saturday_ampm'] = _saturday_ampm

        if _sunday_hour:
            context['sunday_hour'] = _sunday_hour

        if _sunday_minute:
            context['sunday_minute'] = _sunday_minute

        if _sunday_ampm:
            context['sunday_ampm'] = _sunday_ampm

        return render_template('cron.html', context=context)


@app.route('/setup/updates', methods=['GET', 'POST'])
def updates():
    global _update_day_of_week
    global _update_hour
    global _update_minute
    global _update_ampm
    global _everyday

    if request.method == 'POST':

        remove_update_cron()

        update_day_of_week = request.form['day_of_week']
        _update_day_of_week = update_day_of_week

        update_hour = request.form['hour']
        _update_hour = update_hour

        update_minute = request.form['minute']
        _update_minute = update_minute

        update_ampm = request.form['ampm']
        _update_ampm = update_ampm

        cron_string = convert_to_cron(update_hour, update_minute, update_ampm, "apt update -y && apt upgrade -y && apt autoremove -y", day=update_day_of_week, everyday=False)

        try:
            write_update_cron(cron_string)
        except Exception as e:
            print(e)

        return redirect('/setup/close')
    else:
        context = {
            'everyday': _everyday
        }

        if _update_day_of_week:
            context['update_day_of_week'] = _update_day_of_week

        if _update_hour:
            context['update_hour'] = _update_hour

        if _update_minute:
            context['update_minute'] = _update_minute

        if _update_ampm:
            context['update_ampm'] = _update_ampm

        return render_template('updates.html', context=context)


@app.route('/setup/close')
def close():
    print('close executed')
    return render_template('close.html')


if __name__ == '__main__':
    app.run()
